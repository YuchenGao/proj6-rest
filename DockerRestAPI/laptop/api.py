# Laptop Service
from flask import Flask
from flask_restful import Resource, Api
import os
import io
import csv
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import json
from bson import ObjectId



# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

class JSONEncoder(json.JSONEncoder):
    def default(self, o):	
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


class Laptop(Resource):
    def get(self):
    	return "Follow ReadMe instruction to get specific data and format"

        
class listAll(Resource):
	def get(self):
		_items = db.tododb.find()

		items = [item for item in _items]
		for item in items:
			item.pop("_id")
		app.logger.debug(items)
		return JSONEncoder().encode(items)

class listOpenOnly(Resource):
	def get(self):
		_items = db.tododb.find()
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
			item.pop("close")
		app.logger.debug(items)
		return JSONEncoder().encode(items)

class listCloseOnly(Resource):
	def get(self):
		_items = db.tododb.find()
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
			item.pop("open")
		app.logger.debug(items)
		return JSONEncoder().encode(items)

class listAllcsv(Resource):
	def get(self):
		_items = db.tododb.find()
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
		string =io.StringIO()
		writer = csv.writer(string,delimiter=' ', quoting=csv.QUOTE_ALL)
		writer.writerow(items)
		return string.getvalue()

class listOpencsv(Resource):
	def get(self):
		_items = db.tododb.find()
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
			item.pop("close")
		string =io.StringIO()
		writer = csv.writer(string, quoting=csv.QUOTE_ALL)
		writer.writerow(items)
		return string.getvalue()

class listClosecsv(Resource):
	def get(self):
		_items = db.tododb.find()
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
			item.pop("open")
		string =io.StringIO()
		writer = csv.writer(string, quoting=csv.QUOTE_ALL)
		writer.writerow(items)
		return string.getvalue()

#Json Format
api.add_resource(Laptop, '/')
api.add_resource(listAll,'/listAll','/listAll/json')
api.add_resource(listOpenOnly,'/listOpenOnly','/listOpenOnly/json')
api.add_resource(listCloseOnly,'/listCloseOnly','/listCloseOnly/json')

#CSV Format
api.add_resource(listAllcsv,'/listAll/csv')
api.add_resource(listOpencsv,'/listOpenOnly/csv')
api.add_resource(listClosecsv,'/listCloseOnly/csv')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)

